#![allow(dead_code)]
//	path: ./game_name/save_name/metadata.json
use std::{fs};
use std::io::{Write};
use std::path::Path;
use std::time::{SystemTime};
use serde::{Deserialize, Serialize};

pub const BASEPATH: &str = "./";

#[derive(Deserialize, Serialize, Clone)]
pub struct Game {
	pub source: Source,
	pub backups: Vec<String>
}
impl Game {
	pub fn new(name: &String, source_dir: &String) -> Game{
		Game { source: (Source::new(&name, &source_dir)), backups: (Vec::<String>::new()), }
	}
}
#[derive(Deserialize, Serialize, Clone)]
pub struct SmeeData {
	pub games: Vec<Game>
}

impl SmeeData {
	pub fn import() -> SmeeData {
		let settings_file;
		match fs::read_to_string(Path::new(BASEPATH).join("smee.json")) {
			Ok(s) => {
				settings_file = s;
				serde_json::from_str(&settings_file).unwrap()
			},
			Err(_e) => {
				//missing file
				let new = SmeeData {
					games: (Vec::<Game>::new())
				};
				new.export();
				new
			}
		}
	}
	pub fn export(&self) {
		let json = serde_json::to_string_pretty(self).unwrap();
		let mut f = fs::File::create(Path::new(BASEPATH).join("smee.json")).unwrap();
		let _ = write!(&mut f, "{}", json);
	}
}
#[derive(Deserialize, Serialize, Clone)]
pub struct Source {
	pub name: String,
	source_dir: String,
	files: Vec<String>
}

#[derive(Deserialize, Serialize)]
pub struct SaveMetadata {
	pub save: Source,
	pub date: SystemTime
}

impl Source {
	pub fn new(name: &String, source_dir: &String) -> Source {
		Source { name: (name.clone()), source_dir: (source_dir.clone()), files: (Vec::<String>::new()) }

	}
	///update Source.files
	pub fn get_dir(&mut self) {
		match get_dir(&self.source_dir) {
			Some(f) => {
				self.files = f;
			},
			None => println!("Get Directory problem")
		}
	}
	///backup
	pub fn backup(&self, save_name: String) -> Option<String>{
		backup(&self.source_dir, &self.name, &save_name, &self.files, self.clone())
	}
	///restore
	pub fn restore(&self) {
		let backup_dir = Path::new(BASEPATH).join(&self.name).join(&self.name);
		let f = backup_dir.join("metadata.json");
		let json = fs::read_to_string(f).unwrap();
		let metadata: SaveMetadata = serde_json::from_str(&json).unwrap();
		let backup_files = get_dir(&backup_dir.as_os_str().to_str().unwrap().to_owned()).unwrap();
		//clear sourcedir
		let _ = fs::remove_dir_all(Path::new(&metadata.save.source_dir));
		let _ = fs::create_dir_all(&metadata.save.source_dir);
		//copy files
		for file in backup_files {
			let new_file_rel = Path::new(&file).strip_prefix(&metadata.save.source_dir).unwrap();
			let new_file = Path::new(&metadata.save.source_dir).join(new_file_rel);
			let _ = fs::copy(&file, new_file);
		}
	}
}


fn get_dir(source_dir: &String) -> Option<Vec<String>> { 
	match fs::read_dir(source_dir) {
		Ok(files) => {
			let mut dir: Vec<String> = Vec::new();
			for file in files {
				match file {
					Ok(file) => {
						match file.path().into_os_string().into_string() {
							Ok(f) =>  {
								match Path::new(&f).is_dir() {
									true => {
										dir.append(&mut get_dir(&f)?);
									},
									false => dir.push(f)
								}
								
							},
							Err(e) => {
								println!("{:?}", e);
								//"Path Err, can't convert OSstring to String");
								return None;
							}
						}
					},
					Err(e) => {
						println!("{:?}", e);
						return None;
					}
				}			
			}
			return Some(dir);
		},
		Err(e) => {
			println!("{:?}", e);
			return None;
		}
	}
}

fn backup(source_dir: &String, game_name: &String, save_name: &String, files: &Vec<String>, source: Source) -> Option<String>{
	let new_dir = Path::new(BASEPATH).join(&game_name).join(&save_name);
	let mut dates = Vec::<(u64, SystemTime)>::new();
	for file in files{
		let file_path = Path::new(&file);
		match file_path.is_file() {
			true => {
				let date = fs::metadata(file_path).unwrap().modified().unwrap();
				dates.push((date.elapsed().unwrap().as_secs(), date));
				let rel_path = file_path.strip_prefix(&source_dir).unwrap();
				let new_path = new_dir.join(rel_path);
				let _  = fs::copy(file_path, new_path);
			},
			false => {
				return None;
			}

		}
	}
	dates.sort_unstable();
	let metadata = SaveMetadata {
		save: source,
		date: dates[0].1
	};
	let json = serde_json::to_string_pretty(&metadata).unwrap();
	//write
	let mut f = fs::File::create(new_dir.join("metadata.json")).unwrap();
	let _ = write!(&mut f, "{}", json);
	return Some("ok".to_owned());
}

///retrieves Metadata
pub fn get_data(game_name: &String, save_name: &String) -> SaveMetadata {
    let metadata_file = Path::new(BASEPATH).join(game_name).join(save_name).join("metadata.json");
    let json = fs::read_to_string(metadata_file).unwrap();
    let metadata: SaveMetadata = serde_json::from_str(&json).unwrap();
    metadata
}