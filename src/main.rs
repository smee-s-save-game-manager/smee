use smee_lib::*;
use std::env;
use std::io::{self, BufRead};
use std::path::Path;
use std::fs;


fn main() {
    let mut smeedata = SmeeData::import();
    let v: Vec<String> = env::args().collect();
    match v[1].as_str() {
        "help" | "--help" | "-h"=> {
            println!("[option] [game] [save name]\noptions:\nbackup \nrestore \ngames")
        }
        "backup" | "--backup" | "-b"=> {
            //backup
            
        },
        "restore" | "--restore" | "-r"=> {
            restore(&v[2], &v[3], &smeedata);
        },
        //manage games, unknown games will be added as new
        "games" | "--games" | "-g" => {
            let mut new = true;
            for game in &smeedata.games {
                if v[2] == game.source.name {
                    new = false;
                    //manage game
                    println!("delete (d)\nbackup (b)\nrestore (r)\ncancle (c)");
                    let mut buffer = String::with_capacity(2048);
                    let mut stdin = io::stdin().lock();
                    let _ = stdin.read_line(&mut buffer);
                    match &*buffer {
                        "d" => {
                            let dir = Path::new(smee_lib::BASEPATH).join(&game.source.name);
                            let _ = fs::remove_dir_all(dir).unwrap();
                        },
                        "b" => {

                        },
                        "r" => {
                            buffer.clear();
                            println!("What backup to restore from \n {:?}", game.backups);
                            let _ = stdin.read_line(&mut buffer);
                            if !&game.backups.contains(&buffer) {
                                println!("Not an option");
                            }
                            for backup in &game.backups {
                                if &buffer == backup {
                                    
                                }
                            }
                        },
                        "c" => {

                        },
                        _ => {
                            println!("not an option");
                        }
                    }
                    buffer.clear();

                    //stdin.read_line(&mut buffer);
                }
            }
            if new {
                let mut buffer = String::with_capacity(2048);
                let mut stdin = io::stdin().lock();
                println!("Add new game? (Y/n)");
                let _ = stdin.read_line(&mut buffer);
                match &*buffer {
                    "n" | "no" | "N" | "NO" => {
                        //exit
                    },
                    "y" | "yes" | "Y" | "YES" | _ => {
                        
                        buffer.clear();
                        println!("path?");
                        let _ = stdin.read_line(&mut buffer);
                        match Path::new(&buffer).exists() {
                            true => {
                                smeedata.games.push(smee_lib::Game::new(&v[2], &buffer));
                            },
                            false => {
                                println!("Path not found, try again {}", &buffer);
                            }
                        }
                    }
                }
                buffer.clear();
            }
        },
        _ => {
            println!("I don't know what to do cap'n");
            println!("try -h")
        }
    }
    smeedata.export();
    
}


fn restore(game_name: &String, backup_name: &String, smeedata: &SmeeData) {
    let games = &smeedata.games;
    for game in games {
        if game_name == &game.source.name {
            for backup in &game.backups {
                if &backup_name == &backup {
                    let metadata = smee_lib::get_data(&game.source.name, &backup);
                    metadata.save.restore();
                }
            }
        }
    }
}